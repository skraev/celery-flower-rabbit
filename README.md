1. configure DB settings in `configs/celery.cfg`
2. `./run.sh`
3. wait 1-3 minutes
4. check http://127.0.0.1:5555/
5. post some tasks to one of queues, e.g.

```
docker container exec -it smart-space_celery-payments_1 /bin/sh
```

```python
from smartspace.celery_app import app
from smartspace.async_tasks.user_events import *

clear_submit_meter_readings_events_important.delay(1)
clear_submit_meter_readings_events_important.apply_async(args=(1,), countdown=100)
```

6. check flower monitoring http://127.0.0.1:5555/
