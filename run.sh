source ./.env &>/dev/null || true
docker volume rm $CONFIGS_VOLUME || true
docker volume create $CONFIGS_VOLUME
if [ ! "$(docker volume ls -q -f name=${FILE_STORAGE_VOLUME})" ]; then
    docker volume create $FILE_STORAGE_VOLUME
fi

docker run --rm -v `pwd`/configs:/tmp/smart-space -v configs:/etc/smart-space busybox cp -Rv /tmp/smart-space/ /etc/

docker-compose up -d

